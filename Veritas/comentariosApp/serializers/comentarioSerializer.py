from django.db import models
from comentariosApp.models.comentario import Comentario
from rest_framework import serializers
class ComentarioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comentario
        fields = ['id', 'texto', 'tiempo']

class ReadCommentSerializer(serializers.ModelSerializer):
    id = serializers.CharField(read_only=True)
    texto = serializers.CharField(read_only=True)
    tiempo = serializers.SerializerMethodField(read_only=True)
   
