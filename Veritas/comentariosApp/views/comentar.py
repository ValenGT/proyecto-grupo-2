from re import X
from rest_framework.views import APIView
from rest_framework import generics,status
from rest_framework.response import Response


from serializers import comentarioSerializer
from models import comentario


class CommentCreate(generics.CreateAPIView):

 def post(self, request, *args, **kwargs):
        serializer = comentarioSerializer.ComentarioSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()  
           
        return Response('', status=status.HTTP_201_CREATED)   

class CommentView(generics.RetrieveAPIView):
    queryset = comentario.objects.all()
    serializer_class = comentarioSerializer.ReadCommentSerializer
   

    def get(self, request, *args, **kwargs):
                           
        return super().get(request, *args, **kwargs)

class RemoveComment(APIView):
   
    queryset = comentario.objects.all()
    serializer_class = comentarioSerializer.ComentarioSerializer
    
    def delete(self):
        record = comentario.objects.get(id = X)
        return record.delete()

class UpdateComment(APIView):

    queryset = comentario.objects.all()
    serializer_class = comentarioSerializer.ComentarioSerializer
    
    def put(self):
       
        self.date_created = comentario.Comentario.texto()
        return super('' , self).save()
   

