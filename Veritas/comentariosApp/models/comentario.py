from django.db import models
from .user import User

class Comentario(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='comentario', on_delete=models.CASCADE)
    texto   = models.TextField(verbose_name="Comentario")
    tiempo = models.DateTimeField(auto_now_add=True)
    
